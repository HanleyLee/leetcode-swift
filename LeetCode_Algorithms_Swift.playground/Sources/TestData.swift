import Foundation

public let intNumArr = [90, 4, 2, 7, 99, 0, 77, 4]
public let doubleNumArr = [1.3, 0.2, 0.99, 0.00, 4.23, 4.20]
public let mixNumArr = [3, 5, 6, 9, 10, 4, 4, 2.5, 1, 4]
public let textArr = ["李", "鸿", "磊"]
